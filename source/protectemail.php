<?php
 
// Kein direkter Zugriff erlauben
defined( '_JEXEC' ) or die( 'Restricted access' );

class plgContentProtectemail extends JPlugin {
	
	public static function onContentPrepare($context, $article) {

		// Definition E-Mail
		$email_blank = '[^@:="\'\s]*@[^@\s.]*\.[a-z]+';
		// REGEXP E-Mail
		$email = '/' . $email_blank . '/i';

		if($article->text AND preg_match($email,$article->text) == true) {

			if(!function_exists('auslesen_email')) {
				function auslesen_email($email) { 
					if($email[1] == $email[2]) {
						return $email[2];
					} else {
						return $email[2] . ' (' . $email[1] . ')';
					}
				}
			}
			// Mail-Links löschen
			// Letzter Parameter je nach CMS
			$artikel = preg_replace_callback('/<a.*(' . $email_blank . ')[^a-z].*>(.*)<\/a>/iU','auslesen_email',$article->text);
		

			if(!function_exists('umbenennen_email')) {
				function umbenennen_email($email) { 
					return str_replace('@','<span title="Dieses Zeichen vor dem Versand mit einem @-Zeichen ersetzen." style="color:#516da1; cursor: help;">✉</span>',$email[0]);
				}
			}

			// E-Mail ersetzen
			$Fertig = preg_replace_callback($email, 'umbenennen_email', $artikel);

		
			// Je nach CMS unterschiedlich
			// Im Rahmen des Plugins ein $article->text
			$article->text = $Fertig;
		} else {
			$article->text = $article->text;
		}

	}
}
